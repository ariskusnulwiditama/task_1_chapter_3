
CREATE DATABASE IF NOT EXISTS `dbtugas` 
USE `dbtugas`;


DROP TABLE IF EXISTS `tblcustomer`;
CREATE TABLE IF NOT EXISTS `tblcustomer` (
  `id_customer` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;


DELETE FROM `tblcustomer`;
INSERT INTO `tblcustomer` (`id_customer`, `nama`, `alamat`, `id_order`) VALUES
	(1, 'joko', 'jl', 1),
	(2, 'kiki', 'ji madu 45 palembang', 2),
	(3, 'maya', 'jl kalimantan 23 surabaya', 3),
	(4, 'budi', 'jl gajahmada 22', 4);

DROP TABLE IF EXISTS `tbldetailorder`;
CREATE TABLE IF NOT EXISTS `tbldetailorder` (
  `id_order` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  KEY `id_order` (`id_order`),
  KEY `id_customer` (`id_customer`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DELETE FROM `tbldetailorder`;
INSERT INTO `tbldetailorder` (`id_order`, `id_customer`, `id_product`, `total`, `date`) VALUES
	(1, 1, 1, 20, '2022-06-06 23:52:48'),
	(2, 2, 2, 12, '2022-06-07 00:05:50'),
	(3, 3, 3, 33, '2022-06-07 00:17:28'),
	(4, 4, 4, 10, '2022-06-07 00:19:08');

DROP TABLE IF EXISTS `tblorder`;
CREATE TABLE IF NOT EXISTS `tblorder` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT 0,
  `id_product` int(11) DEFAULT 0,
  PRIMARY KEY (`id_order`),
  KEY `id_customer` (`id_customer`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;


INSERT INTO `tblorder` (`id_order`, `id_customer`, `id_product`) VALUES
	(1, 1, 1),
	(2, 2, 2),
	(3, 3, 3),
	(4, 4, 4);


CREATE TABLE IF NOT EXISTS `tblproduct` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL DEFAULT 0,
  `stock` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;


DELETE FROM `tblproduct`;
INSERT INTO `tblproduct` (`id_product`, `product_name`, `price`, `stock`) VALUES
	(1, 'sabun mandi', 5000, 300),
	(2, 'sikat gigi', 12000, 120),
	(3, 'handuk', 50000, 100),
	(4, 'kaos oblong', 100000, 300);


SELECT * FROM tblorder a INNER JOIN tbldetailorder b ON a.id_order=b.id_order WHERE b.total=33;
SELECT * FROM tblcustomer a WHERE a.id_order=1;
SELECT MAX(a.price) FROM tblproduct a;
SELECT * FROM tblcustomer a LEFT JOIN tbldetailorder b on a.id_customer=b.id_customer LEFT JOIN tblorder c ON b.id_order=c.id_order WHERE b.id_product=4;
SELECT MIN(a.price) FROM tblproduct a; 
SELECT MAX(a.total) FROM tbldetailorder a;
